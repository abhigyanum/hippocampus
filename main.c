#include <stddef.h>
#include <stdio.h>

#define CAPACITY 512

// Declaring packet structure.
typedef struct {
    char* start;
    size_t size;
} packet;

// Allocate a stack with total memory of 0.5 KB.
char STACK[CAPACITY];

// Pointer to keep track of current memory
// location in the stack.
void* TRACK;

// Cleans/removes the all contents of the stack,
// eventually destroying it. Equivalent to
// free().
void MEM_clean();

int main(void) {
    printf("Hello, world");

    MEM_clean();
}

