#include <stdlib.h>
#include <stdint.h>

void* HIPPOCAMPUS_mem(uint16_t KB) {
    return malloc(1024 * KB);
}

void HIPPOCAMPUS_clean(void* mem) {
    free(mem);
}

